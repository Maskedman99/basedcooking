# basedcooking

Script to view recipes from https://based.cooking/ in terminal.  
Submit new recipes here https://github.com/lukesmithxyz/based.cooking

![output.gif](https://gitlab.com/Maskedman99/basedcooking/-/raw/master/output.gif)

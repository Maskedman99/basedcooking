#!/bin/sh

# Menu command
menu="$HOME/.config/scripts/fzfmenu.sh -p Recipe:"

# Default directory used to store the files temporarily
cachedir="/tmp/basedcooking"
[ ! -d "$cachedir" ] && printf "$cachedir does not exist, creating... \n" && mkdir -p "$cachedir"

printf "Fetching list of recipes...\n" & curl -s -H "User-agent: 'your bot 0.1'" "https://based.cooking/" > "$cachedir/based.cooking.html"
grep "<li><a href=\".*html.*" "$cachedir/based.cooking.html" | sed -e "s/<li><a href=\"//" -e "s/<\/a><\/li>//" -e "s/\.html\">/ /" > "$cachedir/basedcooking1"
printf "Received $(wc -l < "$cachedir/basedcooking1") recipes\n"

RECIPE=$(cut -d' ' -f 2- "$cachedir/basedcooking1" | $menu)

if [ -z "$RECIPE" ]; then
  printf "No Recipe selected, exiting...\n"

else
  RECIPEFILE=$(grep -w "$RECIPE" "$cachedir/basedcooking1" | cut -d' ' -f1)
  printf "Fetching recipe $RECIPE...\n" & curl -s -H "User-agent: 'your bot 0.1'" "https://raw.githubusercontent.com/LukeSmithxyz/based.cooking/master/src/"$RECIPEFILE".md" > "$cachedir/$RECIPEFILE.md"

# Coloring and Bolding
  sed -i_bkp -e '0,/# /s/\# \(.*\)/'$(tput bold)$(tput setaf 1)'\1'$(tput sgr0)'/' \
    -e 's/### \(.*\)/'$(tput bold)$(tput setaf 6)'\1'$(tput sgr0)'/' \
    -e 's/## \(.*\)/'$(tput bold)$(tput setaf 4)'\1'$(tput sgr0)'/' \
    -e 's/`\(.*\)`/'$(tput setaf 7)$(tput setab 8)'\1'$(tput sgr0)'/g' \
    -e 's/\*\*\(.*\)\*\*/'$(tput bold)'\1'$(tput sgr0)'/g' \
    -e 's/pix\//https:\/\/raw.githubusercontent.com\/LukeSmithxyz\/based.cooking\/master\/data\/pix\//g' \
    -e '$ s\;tags:\'$(tput setaf 4)'Tags:'$(tput sgr0)'\' "$cachedir/$RECIPEFILE.md"

# Print to terminal
  printf "\n%s\n\n" "$(<"$cachedir/$RECIPEFILE.md")"

fi

# Once finished, remove all of the cached files
rm "${cachedir:?}"/*
